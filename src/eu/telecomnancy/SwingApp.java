package eu.telecomnancy;

import eu.telecomnancy.helpers.ObservableSensor;
import eu.telecomnancy.sensor.ProxySensor;
import eu.telecomnancy.sensor.SensorFactory;
import eu.telecomnancy.sensor.StateSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        ObservableSensor sensor = SensorFactory.makeSensor();
        new MainWindow(sensor);
    }

}
