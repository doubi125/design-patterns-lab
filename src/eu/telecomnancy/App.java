package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacySensorAdapter;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class App {

    public static void main(String[] args) {
    	LegacyTemperatureSensor legSensor = new LegacyTemperatureSensor();	
        ISensor sensor = new LegacySensorAdapter(legSensor);
        new ConsoleUI(sensor);
    }

}