package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.ProxySensor;
import eu.telecomnancy.sensor.SimpleSensorLogger;
import eu.telecomnancy.sensor.StateSensor;
import eu.telecomnancy.ui.ConsoleUI;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:18
 */
public class AppProxy {
    public static void main(String[] args) {
        ISensor sensor = new ProxySensor(new StateSensor(), new SimpleSensorLogger());
        new ConsoleUI(sensor);
    }
}
