package eu.telecomnancy.command;

import eu.telecomnancy.sensor.ISensor;

public interface SensorCommand
{
	public void applyCommand(ISensor sensor);
}
