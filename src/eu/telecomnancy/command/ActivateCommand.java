package eu.telecomnancy.command;

import eu.telecomnancy.sensor.ISensor;

public class ActivateCommand implements SensorCommand
{
	@Override
	public void applyCommand(ISensor sensor)
	{
		sensor.on();
	}
}
