package eu.telecomnancy.command;

import eu.telecomnancy.sensor.ISensor;

public class OffCommand implements SensorCommand
{
	@Override
	public void applyCommand(ISensor sensor)
	{
		sensor.off();
	}

}
