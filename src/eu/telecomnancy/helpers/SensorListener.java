package eu.telecomnancy.helpers;


public interface SensorListener
{
	public void onSensorValueChanged(ObservableSensor sensor);
}
