package eu.telecomnancy.helpers;

import eu.telecomnancy.sensor.ISensor;

public interface ObservableSensor extends ISensor
{
	public void addListener(SensorListener listener);
}
