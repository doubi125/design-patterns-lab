package eu.telecomnancy.ui;

import javax.swing.JLabel;

public class SensorDisplay
{
	private Double val;
	
	public SensorDisplay()
	{
		
	}
	
	public void setValue(Double val)
	{
		this.val = val;
	}
	
	public Double getCelsius()
	{
		return val;
	}
	
	public String getValue()
	{
		return "" + val;
	}
	
	public String getUnit()
	{
		return "°C";
	}
}
