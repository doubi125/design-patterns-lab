package eu.telecomnancy.ui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Properties;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import eu.telecomnancy.command.OffCommand;
import eu.telecomnancy.command.SensorCommand;
import eu.telecomnancy.command.UpdateCommand;
import eu.telecomnancy.helpers.ObservableSensor;
import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.helpers.SensorListener;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class SensorView extends JPanel implements SensorListener {
    private ISensor sensor;

    private SensorDisplayPanel value = new SensorDisplayPanel(new SensorDisplay());
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JComboBox displayBox = new JComboBox(new String[]{"Celsius", "Farenheit", "Entier"});
    
    public SensorView(ObservableSensor c) {
        this.sensor = c;
        c.addListener(this);
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);
        
        ReadPropertyFile rp = new ReadPropertyFile();
        try{
	        final Properties p = rp.readFile("/eu/telecomnancy/commande.properties");
	
	
	        on.addActionListener(new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent e) {
	            	applyCommand("onCommand", p);
	            }
	        });
	
	        off.addActionListener(new ActionListener() {
	        	@Override
	            public void actionPerformed(ActionEvent e) {
	            	applyCommand("offCommand", p);
	            }
	        });
	
	        update.addActionListener(new ActionListener() {
	        	@Override
	            public void actionPerformed(ActionEvent e) {
	            	applyCommand("updateCommand", p);
	            }
	        });
	        
	        JPanel southPanel = new JPanel();
	        southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.Y_AXIS));
		        JPanel buttonsPanel = new JPanel();
		        buttonsPanel.setLayout(new GridLayout(1, 3));
		        buttonsPanel.add(update);
		        buttonsPanel.add(on);
		        buttonsPanel.add(off);
		        southPanel.add(displayBox);
		    southPanel.add(buttonsPanel);
		    displayBox.addActionListener(new ActionListener(){
		    	@Override
		    	public void actionPerformed(ActionEvent evt)
		    	{
		    		switch(displayBox.getSelectedIndex())
		    		{
		    		case 0:
		    			value.setDisplay(new SensorDisplay());
		    			break;
		    		case 1:
		    			value.setDisplay(new FarenheitSensorDisplay());
		    			break;
		    		case 2:
		    			value.setDisplay(new TruncSensorDisplay());
		    			break;
		    		}
		    	}
		    });
        this.add(southPanel, BorderLayout.SOUTH);
        } catch (IOException e) {
        	e.printStackTrace();
        }
    }

	@Override
	public void onSensorValueChanged(ObservableSensor sensor)
	{
		try
		{
			value.setValue(sensor.getValue());
		} catch (SensorNotActivatedException e)
		{
			value.setValue(null);
		}
	}
	
	private void applyCommand(String type, Properties p)
	{
		String commandName = p.getProperty(type);
		try {
			SensorCommand command = (SensorCommand) Class.forName(commandName).newInstance();
			command.applyCommand(sensor);
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	}
}
