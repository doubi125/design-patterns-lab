package eu.telecomnancy.ui;

import javax.swing.JLabel;

public class SensorDisplayPanel extends JLabel
{
	SensorDisplay display;
	public SensorDisplayPanel(SensorDisplay display)
	{
		this.display = display;
		updateValue();
	}
	
	public void setDisplay(SensorDisplay display)
	{
		display.setValue(this.display.getCelsius());
		this.display = display;
		updateValue();
	}
	
	public void updateValue()
	{
		if(display.getCelsius() == null)
			setText("N/A " + display.getUnit());
		else
			setText(display.getValue() + display.getUnit());
	}

	public void setValue(Double value)
	{
		display.setValue(value);
		updateValue();
	}
}
