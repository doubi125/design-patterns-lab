package eu.telecomnancy.ui;

public class FarenheitSensorDisplay extends SensorDisplay
{
	@Override
	public String getValue()
	{
		return ""+(1.8*getCelsius()+32);
	}
	
	@Override
	public String getUnit()
	{
		return "°F";
	}
}
