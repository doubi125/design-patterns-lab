package eu.telecomnancy.sensor;

import eu.telecomnancy.helpers.ObservableSensor;

public class StateSensorFactory extends SensorFactory{

	@Override
	public ObservableSensor getSensor() {
		return new StateSensor();
	}

}
