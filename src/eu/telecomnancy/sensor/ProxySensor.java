package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

import eu.telecomnancy.helpers.ObservableSensor;
import eu.telecomnancy.helpers.SensorListener;

public class ProxySensor implements ObservableSensor
{
	private ObservableSensor sensor;
	private ArrayList<SensorListener> listeners = new ArrayList<SensorListener>();
	private SensorLogger log;
	
	public ProxySensor(ObservableSensor sensor, SensorLogger log)
	{
		this.sensor = sensor;
		this.log = log;
	}
	
	@Override
	public void on()
	{
		
		log.log(LogLevel.INFO, new Date(System.currentTimeMillis()) + " on");
		sensor.on();
	}

	@Override
	public void off()
	{
		log.log(LogLevel.INFO, new Date(System.currentTimeMillis()) + " off");
		sensor.off();
	}

	@Override
	public boolean getStatus()
	{
		System.out.println(new Date(System.currentTimeMillis()) + " getStatus " + sensor.getStatus());
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException
	{
		log.log(LogLevel.INFO, new Date(System.currentTimeMillis()) + " update");
		sensor.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException
	{
		log.log(LogLevel.INFO, new Date(System.currentTimeMillis()) + " getValue " + sensor.getValue());
		return sensor.getValue();
	}

	@Override
	public void addListener(SensorListener listener)
	{
		listeners.add(listener);
		sensor.addListener(listener);
	}
}
