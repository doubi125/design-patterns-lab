package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Random;

import eu.telecomnancy.helpers.ObservableSensor;
import eu.telecomnancy.helpers.SensorListener;

public class StateSensor implements ISensor, ObservableSensor
{
	private SensorState state;
	private ArrayList<SensorListener> listeners = new ArrayList<SensorListener>();
	
	public StateSensor()
	{
		state = new OffSensorState(this);
	}
	
	@Override
	public void on()
	{
		state.setState(true);
	}

	@Override
	public void off()
	{
		state.setState(false);
	}

	@Override
	public boolean getStatus()
	{
		return state.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException
	{
		state.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException
	{
		return state.getValue();
	}
	
	public void setState(SensorState state)
	{
		this.state = state;
	}
	
	private interface SensorState
	{
		public void update() throws SensorNotActivatedException;
		public double getValue() throws SensorNotActivatedException;
		public boolean getStatus();
		public void setState(boolean flag);
	}
	
	private class OffSensorState implements SensorState
	{
		private StateSensor parent;
		
		public OffSensorState(StateSensor parent)
		{
			this.parent = parent;
		}
		
		@Override
		public void update() throws SensorNotActivatedException
		{
			for(SensorListener listener : listeners)
				listener.onSensorValueChanged(parent);
			throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
		}

		@Override
		public double getValue() throws SensorNotActivatedException
		{
			throw new SensorNotActivatedException("Sensor must be activated to get its value.");
		}

		@Override
		public boolean getStatus()
		{
			return false;
		}

		@Override
		public void setState(boolean flag)
		{
			if(flag)
				parent.setState(new WorkingSensorState(parent));
		}
	}
	
	private class WorkingSensorState implements SensorState
	{
		StateSensor parent;
		double value;
		
		public WorkingSensorState(StateSensor parent)
		{
			this.parent = parent;
		}

		@Override
		public void update()
		{
			value = (new Random()).nextDouble() * 100;
			for(SensorListener listener : listeners)
				listener.onSensorValueChanged(parent);
		}

		@Override
		public double getValue() throws SensorNotActivatedException
		{
			return value;
		}

		@Override
		public boolean getStatus()
		{
			return true;
		}

		@Override
		public void setState(boolean flag)
		{
			if(!flag)
				parent.setState(new OffSensorState(parent));
		}
	}

	@Override
	public void addListener(SensorListener listener)
	{
		listeners.add(listener);
	}
}
