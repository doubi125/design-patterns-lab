package eu.telecomnancy.sensor;

import java.io.IOException;
import java.util.Properties;

import eu.telecomnancy.helpers.ObservableSensor;
import eu.telecomnancy.helpers.ReadPropertyFile;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 19:33
 */
public class ProxySensorFactory extends SensorFactory {
    @Override
    public ObservableSensor getSensor() {
        return new ProxySensor(new StateSensor(), new SensorLogger(){
        	@Override
        	public void log(LogLevel level, String message)
        	{
        		if(level == LogLevel.INFO)
        			System.out.println(message);
        		else if(level == LogLevel.ERROR)
        			System.err.println(message);
        	}
        });
    }
}
