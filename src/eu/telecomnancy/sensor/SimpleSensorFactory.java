package eu.telecomnancy.sensor;

import eu.telecomnancy.helpers.ObservableSensor;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 19:35
 */
public class SimpleSensorFactory extends SensorFactory {

    @Override
    public ObservableSensor getSensor() {
        return new LegacySensorAdapter(new LegacyTemperatureSensor());
    }
}
