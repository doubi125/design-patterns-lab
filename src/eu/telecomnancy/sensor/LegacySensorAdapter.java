package eu.telecomnancy.sensor;

import java.util.ArrayList;

import eu.telecomnancy.helpers.ObservableSensor;
import eu.telecomnancy.helpers.SensorListener;

public class LegacySensorAdapter implements ObservableSensor
{
	private LegacyTemperatureSensor sensor;
	private boolean state = false;
	private double value = 0;
	private ArrayList<SensorListener> listeners = new ArrayList<SensorListener>();
	
	public LegacySensorAdapter(LegacyTemperatureSensor sensor)
	{
		this.sensor = sensor;
	}

	@Override
	public void on()
	{
		if(!state)
		{
			sensor.onOff();
			state = true;
		}
		
	}

	@Override
	public void off()
	{
		if(state)
		{
			state = false;
			sensor.onOff();
		}
	}

	@Override
	public boolean getStatus()
	{
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException
	{
		value = sensor.getTemperature();
		for(SensorListener listener : listeners)
			listener.onSensorValueChanged(this);
	}

	@Override
	public double getValue() throws SensorNotActivatedException
	{
		return value;
	}
	
	@Override
	public void addListener(SensorListener listener)
	{
		listeners.add(listener);
	}
}
